export default {

  axios: {
    proxy: true
  },

  build: {
    extend (config, { loaders }) {
      config.module.rules.push({
        test: /\.ya?ml$/,
        type: 'json', // Required by Webpack v4
        use: 'yaml-loader'

      })
    }
  },

  buildModules: ['@nuxtjs/vuetify'],

  components: true,

  content: {
    dir: '../../bycco_shared/content'
  },

  css: [],

  env: {
    google_client_id: process.env.GOOGLE_CLIENT_ID ||
      '464711449307-7j2oecn3mkfs1eh3o7b5gh8np3ebhrdp.apps.googleusercontent.com'
  },

  generate: {
    fallback: true,
    dir: 'dist/',
    routes: [
      '/pairings/u8',
      '/pairings/u10',
      '/pairings/u12',
      '/pairings/u14',
      '/pairings/u16',
      '/pairings/u18',
      '/pairings/u20'
    ]
  },

  head: {
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    titleTemplate: '%s - Bycco',
    title: 'bycco'
  },

  i18n: {
    lazy: true,
    locales: [
      { code: 'nl', file: 'nl.js' },
      { code: 'fr', file: 'fr.js' },
      { code: 'de', file: 'de.js' },
      { code: 'en', file: 'en.js' }
    ],
    langDir: 'lang/',
    strategy: 'prefix_and_default',
    defaultLocale: 'nl',
    vueI18n: {
      silentTranslationWarn: false,
      silentFallbackWarn: true
    }
  },

  markdownit: {
    html: true
  },

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/i18n',
    '@nuxtjs/markdownit',
    '@nuxt/content',
    ['nuxt-vuex-localstorage', { localStorage: ['token'] }]
  ],

  plugins: [{ src: '~plugins/api', ssr: false }],

  proxy: {
    // '/api/': 'https://www.bycco.be'
    '/api/': 'http://localhost:8000'
  },

  publicRuntimeConfig: {
    axios: {
      browserBaseURL: process.env.BROWSER_BASE_URL
    }
  },

  srcDir: 'src/',

  target: 'static',

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      light: true
    }
  }

}
