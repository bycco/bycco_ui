import PaymentRequest from '@/api/paymentrequest'
import Reservation from '@/api/reservation'
import Root from '@/api/root'
import Enrollment from '@/api/enrollment'
import Attendee from '@/api/attendee'

export default (context, inject) => {
  const factories = {
    reservation: Reservation(context),
    paymentrequest: PaymentRequest(context),
    root: Root(context),
    enrollment: Enrollment(context),
    attendee: Attendee(context)
  }
  inject('api', factories)
}
