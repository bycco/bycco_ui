
export const state = () => ({
  consentNat: false,
  consentPriv: false,
  enrollment: {},
  step: 1,
  photo: null
})

export const mutations = {
  restart(state) {
    state.consentNat = false
    state.consentPriv = false
    state.photo = null
    state.enrollment = {}
    state.step = 1
  },
  updateConsentNat(state, value) {
    state.consentNat = value
  },
  updateConsentPriv(state, value) {
    state.consentPriv = value
  },
  updatePhoto(state, value) {
    state.photo = value
  },
  updateEnrollment(state, value) {
    state.enrollment = value
  },
  updateStep(state, value) {
    state.step = value
  }
}
