const empytGuest = {
  birthday: '',
  first_name: '',
  last_name: '',
  player: false
}

export const state = () => ({
  address: '',
  daybefore: false,
  dayafter: false,
  email: '',
  first_name: '',
  guestlist: [{ ...empytGuest }],
  last_name: '',
  lodging: '',
  meals: '',
  mobile: '',
  remarks: '',
  step: 1
})

function addEmptyGuest (state) {
  const last = state.guestlist[state.guestlist.length - 1]
  if (!last || last.last_name !== '' || last.first_name !== '') {
    state.guestlist.push({ ...empytGuest })
  }
}

export const mutations = {
  restart (state) {
    state.address = ''
    state.daybefore = false
    state.dayafter = false
    state.email = ''
    state.first_name = ''
    state.guestlist = [{ ...empytGuest }]
    state.last_name = ''
    state.lodging = ''
    state.meals = ''
    state.mobile = ''
    state.remarks = ''
    state.step = 1
  },
  updateAddress (state, value) {
    state.address = value
  },
  updateDayafter (state, value) {
    state.dayafter = !state.dayafter
  },
  updateDaybefore (state) {
    state.daybefore = !state.daybefore
  },
  updateEmail (state, value) {
    state.email = value
  },
  updateFirstName (state, value) {
    state.first_name = value
  },
  deleteGuest (state, ix) {
    state.guestlist.splice(ix, 1)
    addEmptyGuest(state)
  },
  updateGuestsField (state, value) {
    state.guestlist[value.ix][value.field] = value.value
    addEmptyGuest(state)
  },
  updateLastName (state, value) {
    state.last_name = value
  },
  updateLodging (state, value) {
    state.lodging = value
  },
  updateMeals (state, value) {
    state.meals = value
  },
  updateMobile (state, value) {
    state.mobile = value
  },
  updateRemarks (state, value) {
    state.remarks = value
  },
  updateStep (state, value) {
    state.step = value
  }
}
