const categories = [
  { value: 20, text: '-20', year: 2003 },
  { value: 18, text: '-18', year: 2005 },
  { value: 16, text: '-16', year: 2007 },
  { value: 14, text: '-14', year: 2009 },
  { value: 12, text: '-12', year: 2011 },
  { value: 10, text: '-10', year: 2013 },
  { value: 8, text: '-8', year: 2015 }
]

const doctypes = [
  ['page', 'Page'],
  ['article', 'Article'],
  ['calendar', 'Calendar item']
]

const languages = ['nl', 'fr', 'de', 'en']

const yesno = [
  { value: true, text: 'Yes' },
  { value: false, text: 'No' }
]
const canbechampion = [
  'Fide Belg.', 'No Belg.', 'Unknown'
]

const trndates = {
  startdate: new Date('2023-02-19'),
  // enrollment_start: new Date('2022-10-27 20:00'),
  enrollment_start: new Date('2022-10-25 20:00'),
  enrollment_end: new Date('2023-01-15 24:00'),
  lodging_start: new Date('2022-10-23 20:00'),
  // lodging_start: new Date('2022-10-27 20:00'),
}

export { categories, doctypes, languages, yesno, canbechampion, trndates }
