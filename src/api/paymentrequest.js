export default context => ({
  get_paymentrequests (options) {
    const { token } = options
    return context.$axios.get('/api/v1/paymentrequest', {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  get_paymentrequest (options) {
    const { id, token } = options
    return context.$axios.get(`/api/v1/paymentrequest/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  email (options) {
    const { id, token } = options
    return context.$axios.post(`/api/v1/prq/${id}/email`, {}, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  update_paymentrequest (options) {
    const { id, token, ...prupdate } = options
    return context.$axios.put(`/api/v1/paymentrequest/${id}`, prupdate, {
      headers: { Authorization: `Bearer ${token}` }
    })
  }

})
