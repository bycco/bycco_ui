export default context => ({
  add_attendee (options) {
    const { token, ...att } = options
    return context.$axios.post('/api/v1/attendees', att, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  delete_attendee (options) {
    const { id, token } = options
    return context.$axios.delete(`/api/v1/attendee/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  get_attendee (options) {
    const { id, token } = options
    return context.$axios.get(`/api/v1/attendee/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  get_attendees (options) {
    const { token } = options
    return context.$axios.get('/api/v1/attendees', {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  update_attendee (options) {
    const { id, token, ...att } = options
    return context.$axios.put(`/api/v1/attendee/${id}`, att, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  upload_photo (options) {
    const { id, photo, token } = options
    return context.$axios.post(`/api/v1/attendee/${id}/photo`, { photo }, {
      headers: { Authorization: `Bearer ${token}` }
    })
  }

})
