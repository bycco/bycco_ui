export default context => ({
  find_belplayer (options) {
    const { idbel } = options
    return context.$axios.get(`/api/a/enrollment/${idbel}/check`)
  },
  create_enrollment (options) {
    const { enrollment } = options
    return context.$axios.post('/api/a/enrollments/add', enrollment)
  },
  upload_photo (options) {
    const { idsub, photo } = options
    return context.$axios.post(`/api/a/enrollment/${idsub}/photo`, { photo })
  },
  confirm_enrollment (options) {
    const { idsub } = options
    return context.$axios.post(`/api/a/enrollment/${idsub}/confirm`, {})
  },
  anon_enrollments () {
    return context.$axios.get('/api/a/enrollments')
  },
  get_enrollment (options) {
    const { id, token } = options
    return context.$axios.get(`/api/v1/enrollment/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  get_enrollments (options) {
    const { token } = options
    return context.$axios.get('/api/v1/enrollment', {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  xls_enrollments (options) {
    const { token } = options
    return context.$axios.get('/api/v1/xls/enrollment', {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  update_enrollment (options) {
    const { id, token, ...enr } = options
    return context.$axios.put(`/api/v1/enrollment/${id}`, enr, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  create_pr (options) {
    const { id, token, admincost } = options
    return context.$axios.post(`/api/v1/enr/paymentrequest/${id}`, null, {
      headers: { Authorization: `Bearer ${token}` },
      params: { admincost: admincost ? '1' : '0' }
    })
  },
  delete_pr (options) {
    const { id, token } = options
    return context.$axios.delete(`/api/v1/enr/paymentrequest/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  }

})
