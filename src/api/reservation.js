export default context => ({
  add_reservation (options) {
    const { reservationin } = options
    return context.$axios.post('/api/v1/anon/reservation', reservationin)
  },
  get_reservations (options) {
    const { token } = options
    return context.$axios.get('/api/v1/reservation', {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  get_reservation (options) {
    const { id, token } = options
    return context.$axios.get(`/api/v1/reservation/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  update_reservation (options) {
    const { id, token, reservation } = options
    return context.$axios.put(`/api/v1/reservation/${id}`, reservation, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  get_rooms (options) {
    const { token } = options
    return context.$axios.get('/api/v1/room', {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  get_room (options) {
    const { id, token } = options
    return context.$axios.get(`/api/v1/room/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  get_free_rooms (options) {
    const { roomtype } = options
    return context.$axios.get(`/api/v1/freeroom/${roomtype}`)
  },
  assign_room (options) {
    const { id, roomnumber, roomtype, token } = options
    const rtq = roomtype ? `?roomtype=${roomtype}` : ''
    return context.$axios.put(`/api/v1/rsv/assignroom/${id}/${roomnumber}${rtq}`, {}, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  unassign_room (options) {
    const { id, roomnumber, token } = options
    return context.$axios.delete(`/api/v1/rsv/assignroom/${id}/${roomnumber}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  create_pr (options) {
    const { id, token } = options
    return context.$axios.post(`/api/v1/rsv/paymentrequest/${id}`, {}, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  delete_pr (options) {
    const { id, token } = options
    return context.$axios.delete(`/api/v1/rsv/paymentrequest/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  update_pr (options) {
    const { id, paymentrequest, token } = options
    return context.$axios.put(`/api/v1/rsv/paymentrequest/${id}`, paymentrequest, {
      headers: { Authorization: `Bearer ${token}` }
    })
  },
  xls_reservations (options) {
    const { token } = options
    return context.$axios.get('/api/v1/xls/reservation', {
      headers: { Authorization: `Bearer ${token}` }
    })
  }
})
